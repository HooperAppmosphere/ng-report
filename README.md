# README #

### What is this repository for? ###

This is for the Global storm Reporting App. each graph/table/chart is its own self contained angular directive
This is an angular app, so I would recommend heading over to their site and spending some time [learning about the basics](https://angularjs.org/)
### How do I get set up? ###

To get up and running you need a local server to host the files. If you have sublime text you can run a simple sublime server

# CONTROLLERS #
Controllers are where all your data lives in angular. Do your best to keep these free or data manipulation or complex functions. anything more than a few lines should be regulated to a factory or a service.

-the parent controller is the **CustomerReportApp controller.** It has the APIKey, as well as most of the login variables needed for api requests such as the auth key. It is also the home of the user data

-**Customer Report Config** is how we configure the routes for our webapp. there is no links to other pages, views are just swapped out via the customerReport Config. 

-**EOD report controller** is the controller that holds all the information on the end of day report. It is a child of CustomerReportAPP controller. Due to the complex nature of the report data we get back from the server we have multiple variations of that data such as $scope.report, $scope.reportGraphReady, and $scope.tableArray. report is the raw report JSON and the rest are parsed versions of that same data. 


-**Hourly Report Controller** is the controller that holds the data for hourly reports. parts could probably be refactored to keep it slimmer and less logicy, but it works for now. getStartDate is the function that gets fed into the datedpicker directive. same for the get endDate. $scope.tableCategories is the way we get the header info for the hourly report table. you can add whatever value you want from the JSON there and it will be a header in the table. (Logic for this is inside the hourlyreport factory.) These values are reused in the rest of the table, not just the header. you can add filters dynamically to the table here.

```
#!javascript

{
                Name:"Date",
                key:"time"
                filter:"date",
                args:['MM-dd-yy'],
                parse:function(time){
                    return time*1000
                }
```
The above will means that there will be a column in the table called "date". The data in that column will be pulled from the raw data returned from the server and have the key "time" (hence the key named "key"). the filter key represents an angular filter. If you wanted to make custom filters you would pass the name of that filter in here. The args key is the arguments which will be passed to the date filter. Finally parse is how you want the data to be parsed between the raw data, and the date filter. in this case the time needs to be multiplied by 1000 before it is in the proper format for the date filter.
$scope.barDataParse is the function that is passed into the vert bar chart directive. It makes sure that the data is structured, and parsed in a way that wont make the graph die.

# FACTORIES #
Factories are where the more complex functions live. If you are writing more than a few lines of code in your controller you should move it to a factory. Factories return an object where each key is a reference to a function. If you need to pass data around controllers factoreis are the best place to do it. You do this by setting a variable in the factory. ex: var report = []. When you get actual data back from the server you set report to the data returned, then when you need it in a controller you call getReport or some such thing that returns the report.

## AuthFactory ##
This is a pretty basic factory, used for logging into the app. The name of the factory (what you pass into your controller) is "Auth". You will see the default header/param stuff in any factory that has an http request going out. This is necessary for dealing with php backends DONT CHANGE IT UNLESS NECESSARY. In the future this may be hidden away in another factory.

* login: takes a user object, an apiKey, a success callback function and a fail callback function. the user object should have an id and a password

## HTTPFactory ##
Another fairly basic factory. The name is 'HTTP' and is used in I think most if not all of the controllers. Has the same defaults and what not you see in AuthFactory, ignore these! DONT CHANGE THOSE UNLESS THE BACKEND CHANGES.
Right now this Factory only has a get method, as I dont know if we want to be able to set data from the webapp. If in the future we do want that functionality this is the factory where that would live.
* get: Basic http request. It takes a url and a callback function.if there is an error it will perform a console.warn. otherwise on success it runs the callback function with the data passed into it. 


# DIRECTIVES #
Directives can be thought of as angular templates. they consist of javascript files which sometimes link to html templates. you can pass variables into directives as if you were adding attributes onto  an html tag. 

## VERT BAR CHART ##

Here is an example of an instance of the vert bar chart directive in the HTML
```
#!html

<div vertbar-chart data=orderedItems
 parse=barDataParse checked=true watch=regraph 
parse-key="Quantity" axis-func=dateDifference xunits="days"></div>
```
angular automatically takes the name (provided in the directive) "vertbarChart" and creates an attribute called "vertbar-chart" which tells our app to use that linked directive.
if you look in the directive code you see that we are setting scope elements on the attributes "data", "checked", "shown", "parse", "axisFunc" and "watch".
in the html you can pass items from the controller. for example data is passed orderedItems, which if you check the hourly report controller, is the data recieved from the server on one of the ajax calls. below I will explain the role of each scoped key in the directive:

* **data:** This is the raw data to be graphed. self explanatory


* **parse:** a function to parse the data into the required structure. At the very minimum the data is a flat object that has a "Name" key and a "Value" Key. If you provide the optional axis-func then you need a "daydifference" key as well as a "time" key. The value key should be a number. If you want to graph only selected items then you need to include a "checked" bool.

* **parse-key:** technically an attribute, this is a string that gets passed into the above parse function. it is the key that you are graphing. for example if you change it to "price" it your parse function will be fed the string "price" which ideally would return the value you are looking for
 
*  **shown:** This is a left over attribute from the end of day report In case you have your different graphs tabbed, this saves it from rendering (and throwing errors) when you are looking at a different tab. You can just exclude it if you dont have a tabbed view, the default evaluates to true.

 
* **checked:** If you only want "checked" items to be graphed then set this to true. It looks through the parsed data object for items that have the "checked" attribute set to true


* **watch:** what variable you should watch to regraph. if this variable changes then the graph with redraw itself. It defaults to data, but be careful, this can cause it to redraw too frequently and stack overflow. I recomend setting a variable in your controller that simply signals a desire to regraph.


* **axis-func:** This will most likely change as this code matures. It was created to try and solve a problem with graphing across date ranges.  If you are not doing that you can just ignore this. It is essentially a function that will help you format the x axis.

* **xunits:** this is technically an attribute of the directive. It is a string that gets passed into the axis-function


  
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact