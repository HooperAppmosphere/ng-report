//idea based on this one here
//http://www.ng-newsletter.com/posts/d3-on-angular.html

customerReportApp.directive('vertbarChart',['d3Service', '_', '$window',  function(d3Service, _, $window){
    return{
        restrict:'A',
        scope:{
            data:"=",
            checked:"=",
            shown:"=",
            parse:"=",
            axisFunc:"=",
            watch:'='
        },
        link:function(scope, element, attrs){

            var margin = parseInt(attrs.margin)||20;
            var topPadding = parseInt(attrs.toppadding)||50;
            var barPadding = parseInt(attrs.barpadding)||5;
            var svgHeight = parseInt(attrs.svgheight)||"50%";

            var svg = d3.select(element[0]).append("svg")
                .style(
                {
                    width: '100%',
                    height: svgHeight
                });

            //Browser Resize event
            window.onresize=function(){
                scope.$apply()
            };

            var watch = 'watch';
            if(scope.watch ==undefined){
                watch = 'data';
            }


            //watch for the resize event then rerender
            scope.$watch(function(){
                return angular.element($window)[0].innerWidth;
            }, function(){
                scope.render(scope.data)
            });



            //when the data changes we rerender
            scope.$watch(watch, function(newVals, oldVals) {
                if(scope.shown|| scope.shown===undefined){
                    return scope.render(newVals);
                }
            }, true);

            //watch for the tab switch
            if(scope.shown!==undefined) {//careful is shown is not a given value.
                scope.$watch('shown', function () {
                    if (scope.shown) {
                        return scope.render(scope.data);
                    }
                }, true);
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Render the graphs
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            scope.render = function(data){//custom d3 code
                //remove all previous items before rerender
                svg.selectAll('*').remove();

                //if we dont pass any data, return out of the element
                if(scope.parse){
                    data = scope.parse(attrs.parseKey);
                }
//                console.log(data,"readme");
                //only showing data we select
                if(scope.checked)data= _.where(data,{checked:true});

                //we have data so we can graph stuff
                values = _.pluck(data, 'Value');

                //xaxis
                if(scope.axisFunc) {
                    axis = {
                        xAxis : scope.axisFunc(attrs.xunits).difference,
                        data: scope.axisFunc(attrs.xunits).data
                    };
                }
                else {
                    axis = {
                        xAxis: values.length,
                        data: data
                    };
                }

                //have to do this since the x-axis might scale to dates.
                var dateScale = function(d,i){
                    return i;
                };


                if(scope.axisFunc){//if we have a function to scale the axis...do that
                    dateScale= function(d){
                        return Math.floor(scope.axisFunc().compare(scope.axisFunc().start, d.time).difference)-1;
                    };
                }
                //variables
                var width = d3.select(element[0]).node().offsetWidth - margin,
                    height = d3.select(element[0]).node().offsetHeight - topPadding,
                    color = d3.scale.category20(),

                    xScale = d3.scale.ordinal()
                        .domain(d3.range(0, axis.xAxis))
                        .rangeBands([0, width], .1),

                    heightScale = d3.scale.linear()
                        .domain([0, d3.max(values, function (d) {
                            return d*1.5;
                        })])
                        .range([0, height]);



//                console.log(data)
                svg.selectAll('rect')
                        .data(data).enter()
                        .append('rect')
                        .attr('x', function (d, i) {
                            return xScale(dateScale(d,i))
                        })
                        .attr('fill', function (d) {
                            return color(d.Value)
                        })
                        .attr('height', 0)
                        .attr('width', xScale.rangeBand())
                        .attr('y', 0)
                        .on("mouseover",function(d, i ){
                                svg.selectAll('text#detail').remove()
                                svg.append('text')
                                .attr("id",'detail')
                                .attr('x',function(){
                                    return xScale(dateScale(d,i));
                                })
                                .attr("y",function(){
                                    return height - heightScale(d.Value);
                                })
                                .text(function(){
//                                        console.log(d)
                                    return d.Value.toFixed(2);
                                });
//                            return scope.hoverData({data:d})
                        })
                        .transition()
                        .duration(1000)
                        .attr('y', function (d, i) {
                            //console.log(d.Value);
                            return height - heightScale(d.Value)
                        })
                        .attr('height', function (d) {
                            return heightScale(d.Value)
                        })
                    ;
//                }


                var xAxis = d3.svg.axis()
                    .scale(xScale)
                    .orient("bottom")
                    .tickFormat(function(d){
                        return axis.data[d];
                    });

                svg.append("g")
                    .attr("class","axis")
                    .attr("transform", "translate(0,"+height+")")
                    .call(xAxis)
                    .selectAll("text")
                    .style("text-anchor", "end")
                    .attr("dx", "-.8em")
                    .attr("dy", ".15em")
                    .attr("transform", function(d) {
                        return "rotate(-65)"
                    });

                //label x axis
//                svg.selectAll('text')
//                    .data(axis.data).enter()
//                    .append('text')
//                    .attr('transform', 'rotate(90)')
//                    .attr('fill','black')
//                    .attr('x',height+topPadding/2-20)//weird shit due to rotating in svg. it sucks
//                    .attr('y', function(d,i){
//                        return -(xScale(i));//+xScale.rangeBand()/2)
//                    })
//                    .text(function(d){
//                        return  d;
//                    });


            }
        }
    }
}]);
