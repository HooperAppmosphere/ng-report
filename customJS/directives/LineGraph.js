customerReportApp.directive('lineGraph',['d3Service', '_', '$window',  function(d3Service, _, $window) {
    return{
        restrict: 'A',
        scope: {
            data: "=",
            checked: "=",
            shown: "=",
            parse: "=",
            axisFunc: "=",
            watch: '='
        },
        link: function (scope, element, attrs) {

        }
    }
}]);
