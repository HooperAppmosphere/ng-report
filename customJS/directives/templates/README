This is an angular app for sales reports.

angular allows for two way data binding between your html and javascript. you may have noticed the ng-controller tags
in a few divs. that means that all the data and functions inside that div tag have a scope local to the controller
listed in the ng-controller.
if you want to see a particular piece of data all you have to do is wrap it in {{}} in your html
for example in the login.html page there is an error message that is displayed by {{error}}. this corresponds to the
$scope.error variable in the customer report controller.

here is a brief overview of angular logic:

CONTROLLERS:
    -this is where your data lives. try to keep this as small as possible, you dont want to have a whole lot of functions
    living here. strictly for data and calling functions
    -since this is where your data lives this is where the you can look for data populating the HTMl

DIRECTIVES:
    -these are your custom html babies. you can make custom html tags using them, or custom attributes, making the code
    easier to read for someone coming in. for example if someone in this project wants to add a bar graph all they have
     to do is add a div tag with bar-chart attr and a data attribute
        ex: <div bar-chart data=myreport></div>
     here they are passing in data from a controller called myreport. the directive will handle how this is displayed
    -you can make directives that do nothing but refrence a html template.
    -directives return an object. the restrict key is what kind of directive it is. 'E' is for element 'A' for attr and
    'C' for class. for example if you wanted to could change the bar chart one to restrict 'E' and then the html would
    look like
        <bar-graph data=myreport></bar-graph>
    I believe some versions of IE dont like this however
    -you can also make directives do some more impressive things like the barchard directive. It uses D3 to create bar
    graphs of your data. the logic for that lives in the link key of the return object.

FACTORIES:
     -since you dont want a lot of action going on in your controllers this is where they live
     -This is also where you share data between controllers. If you just throw data through scopes at other controllers
     your code will quickly become spaghetti and more difficult to scale later. transfer data between controllers by
     sharing them through a factory or service. for an example of this look at how the ReportsFactory works. data is set
     by the parent customerReport controller, and later that data is retrieved by the EODReport controller.




tutorial on how the bar graphs are working: http://www.ng-newsletter.com/posts/d3-on-angular.html
