customerReportApp.directive('reportTable',function(){
    return{
        restrict:'A',
        scope:{
            categories:'=',
            objectArray:'=',
            click:'='
        },
        link:function(scope, element, attrs){
            scope.sortby="Id";
            scope.reverse = false;
            scope.order=function(name, reverse){
//                console.log(name)
                scope.sortby=name
                scope.reverse = !scope.reverse
            }
        },
        templateUrl:'customJS/directives/templates/hourReportTable.html'
    }
});