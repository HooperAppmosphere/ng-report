customerReportApp.directive('datePicker',['_', function(_) {
    return{
        restrict:'A',
        scope:{
            callback:'=',
            minDate:'=',
            maxDate:'='
        },
        link:function(scope, element, attrs){
            //dynamically change the min and max dates
            scope.$watch('maxDate',function(){
                update();
            });
            scope.$watch('minDate',function(){
                update();
            });

            //updates the min and max dates
            var update=function() {
                var input = $(element[0]);
                input.datepicker("destroy");

                var maxDate = scope.maxDate;
                var minDate = scope.minDate;

                if (scope.maxDate == undefined) {
                    maxDate = "+0d"
                }
                if(scope.minDate==undefined){
                    minDate=""
                }


                input.datepicker({
                    dateFormat: "yy/mm/dd",
                    maxDate: maxDate,
                    minDate:minDate,
                    changeMonth: true,
                    onSelect: function (dateText) {
                        if (scope.callback) {
                            scope.callback(dateText);
                        }
                        scope.$apply();
                    }
                });
            }
        }
    }

}
]);
