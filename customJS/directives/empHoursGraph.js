customerReportApp.directive('vertbarChart',['d3Service', '_', '$window',  function(d3Service, _, $window){
    return{
        restrict:'A',
        scope:{
            data:"=",
            checked:"=",
            shown:"=",
            parse:"=",
            axisFunc:"="
        }
    }
}]);