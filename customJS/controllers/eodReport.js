
customerReportApp.controller('eodReportCtrl',function($scope, EODReports, HTTP){

    $scope.setDate=function(date){
//        console.log(date);
        $scope.reportDate = date;
    };


    var success = function(data){
        //Get rid of this fake data shite once we get some real data to look at.
//        if(parseInt(data.Report.ReportEntries[0].Value)==0){
//            EODReports.set(data);//fakedata
//        }else{
            EODReports.set(data);
//        }

        $scope.report = EODReports.get().Report;

//        console.log($scope.report);

        $scope.reportGraphReady = EODReports.checkBoxReady(EODReports.normalizeGraph($scope.report));

        EODReports.setTableData($scope.reportGraphReady);

        $scope.tableArray=EODReports.getTableData();

        $scope.csv=EODReports.createCSV($scope.reportGraphReady);

        $scope.downloadCSV= function(){
            EODReports.downloadCSV($scope.csv,$scope.report.Type+" "+$scope.reportDate);
        };

        $scope.selectData=function(mydata){
            mydata.checked= !mydata.checked
        };

        $scope.selectedName=function(mydata){
            if(mydata.checked){
                return "bg-success"
            }
            else{
                return ""
            }
        };

        $scope.tips=EODReports.getTips();

        $scope.creditData= EODReports.getCreditData()
        $scope.selectTab(1);
    }



    $scope.selectTab=function(number){
        $scope.tab=number
    };
    $scope.isSelectedTab=function(number){
        return $scope.tab===number
    };
    $scope.setDetail = function(data){
        $scope.detail=data;
        $scope.$apply()
    };

    $scope.$watch('reportDate',function(){
        if($scope.reportDate) {
            var myDate = new Date($scope.reportDate).getTime()/1000;
            var url = "http://208.67.253.237/globalstorm/dev/s/Reports/EndOfDayReportForDateJSON/"+myDate+"/?AuthKey="+$scope.authKey+"&ApiKey="+$scope.ApiKey
            HTTP.get(url, success);
        }
    });
});


var fakeData = {
    "Report" : {
        "Html" : "",
        "Type" : "End of Day",
        "ReportEntries" : [
            {
                "Name" : "Total Sales",
                "Value" : 1000.00
            },
            {
                "Name" : "Total Tickets",
                "Value" : 20
            },
            {
                "Name" : "Total Tips",
                "Value" : 100
            },
            {
                "Name" : "Total Payments",
                "Value" : 800
            },
            {
                "Name" : "Total Discounts",
                "Value" : 20
            },
            {
                "Name" : "Cash Tips",
                "Value" : 0
            },
            {
                "Name" : "Total Cash",
                "Value" : 840
            },
            {
                "Name" : "Total Number of Cash Transactions",
                "Value" : 840
            },
            {
                "Name" : "Total Credit",
                "Value" : 200
            },
            {
                "Name" : "Total Number of Credit Transactions",
                "Value" : 15
            },
            {
                "Name" : "Credit Tips",
                "Value" : 100
            },
            {
                "Name" : "Total Visa",
                "Value" : 180
            },
            {
                "Name" : "Total Number of Visa Transactions",
                "Value" : 5
            },
            {
                "Name" : "Visa Tips",
                "Value" : 90
            },
            {
                "Name" : "Total MasterCard",
                "Value" : 0
            },
            {
                "Name" : "Total Number of MasterCard Transactions",
                "Value" : 0
            },
            {
                "Name" : "MasterCard Tips",
                "Value" : 0
            },
            {
                "Name" : "Total Discover",
                "Value" : "20.00"
            },
            {
                "Name" : "Total Number of Discover Transactions",
                "Value" : 10
            },
            {
                "Name" : "Discover Tips",
                "Value" : "10.00"
            },
            {
                "Name" : "Total American Express",
                "Value" : "0.00"
            },
            {
                "Name" : "Total Number of American Express Transactions",
                "Value" : 0
            },
            {
                "Name" : "American Express Tips",
                "Value" : "0.00"
            }
        ]
    }
};