customerReportApp.controller('employeeCtrl',  function ($scope, _, HTTP, empHourFactory) {
    empHourFactory.setHours();//TODO DELETE THIS ONCE WE HAVE REAL DATA TO GET
    var Success = function(data){
        empHourFactory.setHours(data);

        $scope.rawHours = empHourFactory.getHours();

        $scope.selectItem = function(item){
//            console.log(item);
            item.checked = !item.checked;
            empHourFactory.timeDif(item.Value.StartTime, item.Value.EndTime)
        };
        $scope.tableEmployees=empHourFactory.getTableData($scope.rawHours.Shifts);

        $scope.combinedTableEmployees = empHourFactory.combineHours($scope.tableEmployees);



        $scope.barDataParse=function(key){
            return empHourFactory.getBarData($scope.tableEmployees, key, $scope.startDate, $scope.endDate, "days");
        }

        $scope.tableCategories = [
            {
                Name:"Id",
                key:"Id",
                filter:""
            },
            {
                Name:"Manager Id",
                key:"ManagerId"
            },
            {
                Name:"Hours Worked",
                key:'hoursWorked'
            }

        ]
    };
    $scope.getEmployees = function(){
        HTTP.get(false, Success)
    };
    //Success();//TODO THIS WILL BE PASSED INTO THE HTTP CALLBACK
//    console.log($scope.rawHours);

});
