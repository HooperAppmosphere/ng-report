customerReportApp.controller('hourlyReportCtrl', function($scope, hourlyReportFactory, HTTP){
    $scope.maxDate;
    $scope.regraph = 0;
    $scope.getStartDate=function(date){
//        console.log(new Date(date).getTime());
        var myDate = new Date(date).getTime()/1000;
        $scope.minDate = date;
        $scope.startDate=myDate;
        if($scope.startDate && $scope.endDate){
            $scope.dateReady=true;
        }
        else{
            $scope.dateReady=false;
        }
    };

    $scope.getEndDate=function(date){
//        console.log(date);
        var myDate = (new Date(date).getTime()+24*60*60*1000)/1000;//adding one day because midnight
        $scope.maxDate = date;
//        console.log(myDate);
        $scope.endDate=myDate;
        if($scope.startDate && $scope.endDate){
            $scope.dateReady=true;
        }
        else{
            $scope.dateReady=false;
        }
    };

    $scope.selectItem = function(item, items){
        $scope.regraph +=1;
        _.map(items,function(chosen){
            if(chosen.Value.Id==item.Value.Id){
                return chosen.checked = !chosen.checked;
            }
            else{
                return chosen.checked = false;
            }

        });
//        console.log(item, items);
    };

    $scope.checkAll=false;
    $scope.selectAll = function(){
        $scope.checkAll=!$scope.checkAll;
        _.map($scope.orderedItems,function(item){
            return item.checked=$scope.checkAll;
        });
        $scope.regraph +=1;
    };




    var success=function(data){
        $scope.dailyReady=true;

        hourlyReportFactory.set(data);
        $scope.reports=hourlyReportFactory.get();
//        console.log($scope.reports);

        //getting ready for the table
        hourlyReportFactory.setRestaurantData($scope.reports.Value);

        $scope.paymentGroups=hourlyReportFactory.getRestaurantPaymentGroups();
        $scope.orderedItems=hourlyReportFactory.getRestaurantOrderedItems();
//        console.log($scope.orderedItems);

        //oject used by vertbar chart to get the difference in days
        $scope.dateDifference = function(units){
            var dateDifference = hourlyReportFactory.dateDifference($scope.startDate, $scope.endDate, units);
            return {
                difference : dateDifference.difference,
                start : $scope.startDate,
                end : $scope.endDate,
                data: dateDifference.datesBetween,
                compare:hourlyReportFactory.dateDifference
            }
        };

        //this will live in the bar chart directive. gets the data ready for d3
        $scope.barDataParse=function(key){
            return hourlyReportFactory.getBarData($scope.orderedItems, key, $scope.startDate, $scope.endDate, "days");
        };


//        console.log($scope.barDataParse("Quantity"),"LOOK AT ME");
        //THIS IS ADDINGN UP ALL THE THINGS BY HAND
//        $scope.totalGraphSum = _.reduce(_.pluck($scope.barDataParse("Quantity"),"Value"), function(sum, num) {
//            return sum + num;
//        });
        $scope.totalGraphSum=parseFloat(data.Report.ReportEntries[0].Value.replace(/,/g, ''));

        //this is to get the Headers. add the option of filers later?
        $scope.tableCategories=[
            {
                Name:"Diner Id",
                key:"DinerId",
                filter:""
            },
            {
                Name:"Name",
                key:"Name",
                filter:""
            },
            {
                Name:"Price",
                key:"Price",
                filter:"currency"
            },
            {
                Name:"Quantity",
                key:"Quantity",
                filter:""
            },
            {
                Name:"Date",
                key:"time",
                filter:"date",
                args:['MM-dd-yy'],
                parse:function(time){
                    return time*1000
                }
            }];
        $scope.regraph +=1;

    };



    var url=function(){
        return "https://208.67.253.237/globalstorm/dev/s/Reports/TransactionsForRange/"+$scope.startDate+"/"+$scope.endDate+"?AuthKey="+$scope.authKey+"&ApiKey="+$scope.ApiKey
    };

    $scope.getReport=function(){
//        console.log("clicked");
        console.warn(url());
        HTTP.get(url(), success);
    }



});