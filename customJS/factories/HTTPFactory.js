customerReportApp.factory('HTTP', function($http){


    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function(obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj) {
            value = obj[name];

            if(value instanceof Array) {
                for(i=0; i<value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value instanceof Object) {
                for(subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $http.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];


    return{

        get:function(url, callback){
//            console.log(url);
            if(url){//TODO TAKE THIS OUT ONCE WE HAVE A BACKEND
                console.log("url: ",url)
                $http({
                    method:'Get',
                    url:url
                }).success(function(data){
                    if(data.error || data.Error){
                        if(data.error){
                            console.warn(data.error);
                        }
                        else{
                            console.warn(data.Error)
                        }

                    }
                    else{
                        console.info("Success", data);
                        callback(data);
                    }
                }).error(function(error){
                    console.warn(error)
                });
        }
            else{
                console.warn("no url provided");
                //callback()
            }
        }
    }
});