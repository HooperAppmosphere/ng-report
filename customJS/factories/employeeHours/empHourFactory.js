customerReportApp.factory('empHourFactory',function(){
    employeeHours =[];
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //GET DATA READY FOR HOUR REPORT TABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var tableReady = function(data, concat){
        var tabularData=[];
//        console.log(data);
        _.each(data, function (d) {
            //console.log(d);
            obj = {};
            obj.Value = d;
            obj.Id = d.Id;
            obj.Value.hoursWorked = timeDif(d.StartTime, d.EndTime).hours;
            obj.Value.minsWorked = timeDif(d.StartTime, d.EndTime).mins;
            obj.checked = false;
            tabularData.push(obj)
        });

        if(concat){
//            console.log("concat the data")
            tabularData = concatTime(tabularData);
        }
//        console.log(tabularData);
        return tabularData
    };
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// COMBINE HOURS OF THE SAME EMPLOYEE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var concatTime = function(data){//combines the times
        var placeholder = data;
        data = [];
        ids = _.uniq(_.pluck(placeholder, 'Id'));

        _.each(ids,function(id, index){
            var sameItem = _.where(placeholder,{'Id':id}),
                obj= {
                    Id:id,
                    Value:{
                        startTime: sameItem[0].Value.StartTime,
                        endTime: sameItem[0].Value.EndTime,
                        hoursWorked: 0,
                        minsWorked: 0
                    }
                };

            obj.Value = sameItem[0].Value;
            if(sameItem.length>1) {
                var hours = 0,
                    mins = 0;
                _.each(sameItem, function (item) {//total hours and min and max start and end times
                    hours += item.Value.hoursWorked;
                    mins += item.Value.minsWorked;
                    if (item.Value.StartTime < obj.Value.startTime) {
                        obj.Value.startTime = item.Value.StartTime;
                    }
                    if (item.Value.endTime > obj.Value.endTime) {
                        obj.Value.endTime = item.Value.EndTime;
                    }
                });
                obj.Value.hoursWorked = hours;
                obj.Value.minsWorked = mins;
            }

            obj.Id = id;
            data.push(obj)
        });

        return data;
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //GET THE DIFFERENCE BETWEEN THE START AND END TIMES
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var timeDif = function(start, end){
        var readableStart = new Date( start*1000);
        var readableEnd = new Date( end*1000);
        var difference = readableEnd.getTime()- readableStart.getTime();
        return{
            hours : difference/60/60/1000,
            mins : difference/60/1000
        };
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Get the number of days(eventually other units) between two dates
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var dateDifference = function(start, end, units){
        if(units =="days"){
            multiplier = 24;
        }

        var difference = (end-start)/(60*60*multiplier)+1;
        var datesBetween = [];

        for(var i = start*1000; i<=end*1000; i+=(60*60*multiplier*1000)){
            date = new Date(i);
            datesBetween.push((date.getMonth()+1)+"/"+date.getUTCDate());
        }

        return {
            difference:difference,
            datesBetween:datesBetween
        };
    };


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // prepare an object suitable for bar graphing
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    return{
        setHours :function(data){
            //change this once we have something to hit
            employeeHours = fakeJSON
        },
        getHours :function(){
            return employeeHours
        },
        getTableData:function(data, concat){
            return tableReady(data, concat);
        },
        timeDif:function(start, end){
            return timeDif(start, end);
        },
        dateDifference:function(start, end, units){
            return dateDifference(start, end, units);
        },
        combineHours:function(data){
            return concatTime(data);
        }
    }
});




var fakeJSON = {//delete this later
    Shifts: [
        {
            EndTime: 1415660400,
            Id: "96A8FCAD-7730-43A5-B150-5C421544CAE4",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "74D46C27-643D-4C36-981E-497B9203A38C",
            StartTime: 1415631600,
            Type: 0,
            UserId: "EB04C57B-CFFD-4212-8635-B33CADE65C5A"
        },
        {
            EndTime: 1415746800,
            Id: "977860F2-AC82-4230-8C27-988444C2A2F3",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "74D46C27-643D-4C36-981E-497B9203A38C",
            StartTime: 1415718000,
            Type: 0,
            UserId: "EB04C57B-CFFD-4212-8635-B33CADE65C5A"
        },
        {
            EndTime: 1415919600,
            Id: "ABEF025D-6C10-4A85-BF0B-AB16DAB6B55A",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "74D46C27-643D-4C36-981E-497B9203A38C",
            StartTime: 1415890800,
            Type: 0,
            UserId: "EB04C57B-CFFD-4212-8635-B33CADE65C5A"
        },
        {
            EndTime: 1415833200,
            Id: "BD588E76-CA10-4F3C-9820-9BD58DDE59DA",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "74D46C27-643D-4C36-981E-497B9203A38C",
            StartTime: 1415804400,
            Type: 0,
            UserId: "EB04C57B-CFFD-4212-8635-B33CADE65C5A"
        },
        {
            EndTime: 1415664000,
            Id: "CB2BE765-5285-4C84-B835-9CD352A1283E",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "837BBFC7-C221-495F-AA65-329982589525",
            StartTime: 1415635200,
            Type: 0,
            UserId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4"
        },
        {
            EndTime: 1415836800,
            Id: "D913CCAF-07F8-4DD1-950C-027136A2B599",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "837BBFC7-C221-495F-AA65-329982589525",
            StartTime: 1415808000,
            Type: 0,
            UserId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4"
        },
        {
            EndTime: 1415923200,
            Id: "ED276148-92AA-4BE8-BCD1-8DEC95B51397",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "837BBFC7-C221-495F-AA65-329982589525",
            StartTime: 1415894400,
            Type: 0,
            UserId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4"
        },
        {
            EndTime: 1415750400,
            Id: "FF094902-747E-4EFA-B2E3-A2EA9F9477E7",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "837BBFC7-C221-495F-AA65-329982589525",
            StartTime: 1415721600,
            Type: 0,
            UserId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4"
        },
        {
            EndTime: 1415750400,
            Id: "96A8FCAD-7730-43A5-B150-5C421544CAE4",
            ManagerId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4",
            PunchIn: 0,
            PunchOut: 0,
            RoleId: "837BBFC7-C221-495F-AA65-329982589525",
            StartTime: 1415721600,
            Type: 0,
            UserId: "70BB243F-BF84-49E0-9EEB-1A8070760AF4"
        }
    ]
}