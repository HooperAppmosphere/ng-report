customerReportApp.factory('hourlyReportFactory',function(){
    var report;
    var RestaurantPaymentGroups=[],
        restaurantOrderedItems=[]

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //simple thing. since all the data we get is mixed we want to pull out the transactions object
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var prettyReport=function(data){
        var transactions= _.findWhere(data, {Name:"Transactions"});
        return transactions;
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //dig through the large amount of data for the stuff we care about
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var tableRestaurantData = function(data){
        RestaurantPaymentGroups=[];
        _.each(data,function(d){
            if(d.PaymentGroups.length>0) {
                RestaurantPaymentGroups.push({
                    groupId: d.Id,
                    Value: d.PaymentGroups,
                    displayTime: d.LastUpdated
                });
            }
        });

        RestaurantOrderedItems = []
        _.each(RestaurantPaymentGroups,function(g){
            var displayTime = g.displayTime
//            console.log("Group", g)
            var diners = _.pluck(g.Value,"Diners")[0];
//            console.log(diners)
            _.each(diners,function(diner){
//                console.log("Diner",diner);
                _.each(diner.OrderedItems,function(item){
//                    console.log("item", item)
                    var myItem ={
                            PaymentId: g.groupId,
                            DinerId: diner.Id,
                            Value:item.Item
                    };
//                    console.log(item.Item.PriceQty);
                    myItem.Value.Quantity=item.Quantity;
                    myItem.Value.DinerId=myItem.DinerId;
                    myItem.Value.PaymentId= myItem.PaymentId;
                    myItem.Value.time = displayTime;//item.LastUpdated;
                    myItem.checked =false;
                    myItem.time = displayTime;//item.LastUpdated;
                    RestaurantOrderedItems.push(myItem);
                })
            });
        });
//        console.log(RestaurantOrderedItems, RestaurantPaymentGroups)
    };


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Get the number of days(eventually other units) between two dates
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var dateDifference = function(start, end, units){
        if(units =="days"){
            multiplier = 24;
        }

        var difference = (end-start)/(60*60*multiplier)+1;
        var datesBetween = [];

        for(var i = start*1000; i<=end*1000; i+=(60*60*multiplier*1000)){
            date = new Date(i);
            datesBetween.push((date.getMonth()+1)+"/"+date.getUTCDate());
        }

        return {
            difference:difference,
            datesBetween:datesBetween
        };
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // prepare an object suitable for bar graphing
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var barData = function(data, key, start, end, units){
        var barReady = [];

        _.each(data,function(d){
            obj={};
            obj.Name = d.Value.Name;
            if(key=="Quantity"){
                obj.Value = (parseFloat(d.Value[key])* parseFloat(d.Value.Price));
            }
            else{
                obj.Value = parseFloat(d.Value[key]);
            }
            obj.checked = d.checked;
            obj.time = d.time;
            obj.Id = d.Value.Id;
            barReady.push(obj);
        });


        var dateScale= function(d){
            return Math.floor(dateDifference(start, d.time, units).difference)-1;
        };
        //add the day difference
        _.map(barReady, function(obj){
            obj.daydifference = dateScale(obj)
        });



        var checked = _.where(barReady, {checked:true});
        var dates = _.uniq(_.pluck(checked,"daydifference"));
        _.each(dates, function(date){
            var dateData = _.where(checked, {daydifference:date});
            var combined = 0;
            _.each(dateData, function(d){
                combined+= d.Value
            });
            _.map(barReady,function(d){
                if(d.daydifference==date){
                    return d.Value = combined;
                }
                else{
                    return d.Value*=1
                }
            })
        });

        //get a list of all the unique ids
//        ids = _.uniq(_.pluck(barReady,"Id"));
        //add up the values for the same items that were bought in the same day.
//        _.each(ids, function(id){
//            sameItem = _.where(barReady, {Id:id});
//
//            dates = _.uniq(_.pluck(sameItem,"daydifference"));
//
//            _.each(dates, function(date){
//                dateData = _.where(sameItem, {daydifference:date});
//                combined = 0;
//                _.each(dateData, function(d){
//                    combined+= d.Value
//                });
//                _.map(barReady,function(d){
//                    if(d.Id==id && d.daydifference==date){
//                        return d.Value = combined;
//                    }
//                    else{
//                        return d.Value*=1
//                    }
//                })
//            });
//
//        });

        return barReady;
    };


   return{
       set:function(data){
           report = data;
       },
       getRawReport:function(){
           return report;
       },
       get:function(){
            return prettyReport(report.Report.ReportEntries);
       },
       setRestaurantData:function(data){
           tableRestaurantData(data)
       },
       getRestaurantPaymentGroups:function(){
           return RestaurantPaymentGroups
       },
       getRestaurantOrderedItems:function(){
           return RestaurantOrderedItems
       },
       getBarData:function(data, key, start, end, units){
           return barData(data, key, start, end, units)
       },
       dateDifference:function(start, end, unit){
           return dateDifference(start, end, unit)
       }

   }
});