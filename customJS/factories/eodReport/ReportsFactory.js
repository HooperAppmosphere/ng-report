customerReportApp.factory('EODReports', function(){
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Structuring data
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var cardTips = ["visa tips", "mastercard tips", "discover tips", "american express tips"],
        tipTotals=["cash tips", "total tips","credit tips"],
        creditTransactions=["total number of visa transactions","total number of mastercard transactions",
            "total number of discover transactions", "total number of american express transactions"],
        creditValues=["total mastercard", "total visa", "total discover", "total american express"];

    var myTips={
        creditTips:[],
        totalTips:[],
        tips:[]
        },
        myCreditData={
            creditTransactions:[],
            creditTips:[],
            creditValues:[]
        };
    var reports = [];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////





    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //functions
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //This was used for the reddit data, and also for the first round of reportJSON. feel free to refactor this to suit
    //needs
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var normalizeReports = function(data){
        var dataArray =[];
        _.each(_.pairs(data),function(pair){
            if(_.isNumber(pair[1])){
                var obj = {
                    Name:pair[0],
                    Value:pair[1]
                };
                dataArray.push(obj);
            }
        });
        return dataArray
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //not an overly impressive function... esentially just pulls out the data from ReportEntries key, making it safe for
    //graphs
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var graphReport=function(data){
        data.ReportEntries=_.map(data.ReportEntries,function(d){
            return {
                Name:d.Name,
                Value:parseFloat(d.Value)
            };
        });

        return data.ReportEntries
    };


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get data ready to be put in a table
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var tableResults=[];
    var tableData=function(report){

//        var finalArray=[],
            tableResults=[]
//        console.log(tableResults,"clear results");
            innerArray=[];

        _.each(report,function(data,index){
            if(index%4===0){
                tableResults.push(innerArray);
                innerArray=[];
            }
            innerArray.push(data);
            if(index+1==report.length){
                tableResults.push(innerArray);
            }
        });

//        console.log(tableResults,"set results")
//        tableResults;
//        return finalArray;

    };



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //csv content creation and downloading
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var createCSV = function(data){
        var csvArray=[];
        if(window.navigator.msSaveOrOpenBlob) {
            var csvContent=""
        }else {
            var csvContent = "data:text/csv;charset=utf-8,";
        }
        _.each(data,function(entry){
            var dataString = entry.Name+", "+entry.Value+" ";
            csvArray.push(dataString);
        });
        _.each(csvArray,function(entry, index){
            csvContent += entry+"\n";//index < csvArray.length ? entry+ "\n" : entry;
        });
        return csvContent;
    };


    var downloadCSV = function(csv, name){

        var link = document.createElement("a");
        if(window.navigator.msSaveOrOpenBlob) {//ie
//            console.log("hi IE");
            var fileData = [csv];
            blobObject = new Blob(fileData);
            window.navigator.msSaveOrOpenBlob(blobObject, name+".csv");

        }
        else{
            var encodedUri = encodeURI(csv);
            link.setAttribute("href", "data:text/csv,"+encodedUri);

            if(window.navigator.toSource){//firefox
                name=name.replace(/\s/g, "_");
                link.setAttribute("download", name+".csv");
//                console.log("hello firefox");
                window.open(encodedUri+".csv");
            }
            else{//chrome
                link.setAttribute("download", name+".csv");
                link.click()
            }
        }

    };
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////


    //pull the tips out of the report data
    // this unfortunately relies on magic strings listed above
    var sortData = function(report, names){
        result=[];
        _.each(names,function(name){
            var sortedData = _.find(report,function(entry){
                return entry.Name.toLowerCase() == name;
            });
            if(sortedData) {
                result.push(sortedData);
            }
        });
        return result
    };



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Set the reports so different controllers can access if need be.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var setReports = function(data){
        myTips.creditTips = sortData(graphReport(data.Report),cardTips);
        myTips.totalTips = sortData(graphReport(data.Report), tipTotals);
        myTips.tips=myTips.creditTips.concat(myTips.totalTips);

        myCreditData.creditTransactions=sortData(graphReport(data.Report), creditTransactions);
        myCreditData.creditTips=myTips.creditTips;
        myCreditData.creditValues = sortData(graphReport(data.Report), creditValues);

        reports = data;
    };

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    return{
        set:function(data){
            setReports(data)
        },
        get:function(){
            return reports
        },
        normalizeGraph:function(data){
            return graphReport(data)
        },
        normalizeReports:function(data){
            return normalizeReports(data)
        },
        getTips:function(){
            return myTips
        },
        getCreditData:function(){
            return myCreditData
        },
        checkBoxReady:function(array){
            newArray =  _.map(array, function(value,index){
                value.checked=false;
                return value;
            });
            return newArray;
        },
        setTableData:function(report){
            tableData(report);
        },
        getTableData:function(){
//            console.log("getting results",tableResults)
          return tableResults
        },
        createCSV:function(report){
            return createCSV(report);
        },
        downloadCSV:function(csv, name){
            return downloadCSV(csv, name);
        },
        test:function(){
//            console.log("test")
        }
    };
});
