
var customerReportApp=angular.module("customerReportApp",["d3", "Lodash", "ngRoute"]);

customerReportApp.controller('mainCtrl', function($scope, Auth){
    $scope.showReport=false;

    $scope.ApiKey='0000F129-A738-4AD8-9407-06E6BE44BAB7';



    var success = function(data){//this gets called on success
        $scope.showReport = !$scope.showReport;
        $scope.authKey = data.AuthKey;

        $scope.user = data.User;

//        console.log($scope.user, $scope.authKey);

    };

    var failed = function(data){
        $scope.error = data.Error;
    };

    $scope.toggleReport = function(){ //this will only happen if the login is successful
        Auth.login($scope.login, $scope.ApiKey, success, failed);
    }
});