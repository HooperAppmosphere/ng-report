customerReportApp.filter('tableSort',function(){
    return function(items, key, reversed){
        var filtered;
        if(items) {
            filtered=items.slice(0);
            var multiplier = reversed?-1:1;//enable reverse sorting if you want
            filtered.sort(function (a, b) {
                if (a.Value[key] > b.Value[key]) {
                    return 1*multiplier;
                }
                if (a.Value[key] < b.Value[key]) {
                    return -1*multiplier;
                }
                return 0;
            })
        }
        return filtered;
    }
});