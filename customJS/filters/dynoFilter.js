customerReportApp.filter('dynoFilter',function($filter) {
    //for use in tables and other ng-repeats where you man need dynamically changing filters
//    the "category" passed in to the filter should be of this form
//    {
//        Name:"time",
//        filter:"date",
//        args:['dd-MM-yy'],
//        parse:function(time){
//          return time*1000
//        }
//    }
//    Item here is the element the filter is applied to.
    return function(item, category){
        filtername = category.filter;

        var buildStringArgs = function(array){//can create string arguments for other filters. like date
            str="";
            _.each(array, function(arg){
                str+=arg
            });
            return str
        };
        if(category.parse){//if you have a special function to perform on the item do it.
            item = category.parse(item)
        }

        if(filtername){
            if(category.args){
                return $filter(category.filter)(item, buildStringArgs(category.args));
            }
            else {
                return $filter(category.filter)(item);
            }
        }
        else{
            return item;
        }
    }
});