customerReportApp.config(function($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/EOD-Report',{
            templateUrl:'customJS/Views/EOD_Report.html',
            controller: 'mainCtrl'
        })
        .when('/',{
            templateUrl:'customJS/Views/welcome.html',
            controller:'mainCtrl'
        })
        .when('/Hourly-Report',{
            templateUrl:'customJS/Views/Hourly_Report.html',
            controller:'hourlyReportCtrl'
        });
//        .when('/Employee-Hours',{
//            templateUrl:'customJS/Views/Employee_Hours.html',
//            controller:'employeeCtrl'
//        });

        // route for the about page
//        .when('/about', {
//            templateUrl : 'pages/about.html',
//            controller  : 'aboutController'
//        })
//
//        // route for the contact page
//        .when('/contact', {
//            templateUrl : 'pages/contact.html',
//            controller  : 'contactController'
//        });
});

